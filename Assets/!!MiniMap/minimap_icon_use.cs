﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minimap_icon_use : MonoBehaviour
{
    // Start is called before the first frame 
     public GameObject player;
     private Quaternion SELFROTATE;
    void Start()
    {
        SELFROTATE=Quaternion.Euler(90f,0,0);
        
    }

    // Update is called once per frame
    void Update()
    {
         this.transform.position =  player.transform.position+new Vector3(0,130,0);

       this.transform.rotation=  player.transform.rotation*SELFROTATE;
      
    }
}
