﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

public class minimap_icon_enemy : MonoBehaviour
{
    AI_data data;
    public GameObject thisEmeny;
    void Start()
    {
        data = GetComponentInParent<AI_data>();
        thisEmeny = data.enemy;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = thisEmeny.transform.position + new Vector3(0, 120, 0);
    }
}
