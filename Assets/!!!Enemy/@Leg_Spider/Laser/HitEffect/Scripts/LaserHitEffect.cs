﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserHitEffect : MonoBehaviour
{
    public LaserHitEffectPool laserHitEffectPool;
    

    // Update is called once per frame
    void Update()
    {
        EffectLifeReturnToPool();
    }

    [Header("特效的生命")]
    public float _timer;
    public float effectLife = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        laserHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserHitEffectPool>();
        effectLife = 1.0f;
    }

 
    private void OnEnable()
    {
        _timer = Time.time;
    }

    private void EffectLifeReturnToPool()
    {
        if (!gameObject.activeInHierarchy)
            return;

        if (Time.time > _timer + effectLife)
        {
            laserHitEffectPool.Recovery(this.gameObject);
        }
    }
}
