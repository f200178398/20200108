﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBallShootSoundEffectPool : MonoBehaviour
{    
    void Awake()
    {
        for (int cnt = 0; cnt < initailSize; cnt++)
        {
            GameObject go = Instantiate(prefab) as GameObject;
            m_pool.Enqueue(go);
            go.SetActive(false);
        }
    }


    public GameObject prefab;
    public int initailSize = 69;

    private Queue<GameObject> m_pool = new Queue<GameObject>();



    public GameObject ReUse(Vector3 position, Quaternion rotation)
    {
        if (m_pool.Count > 0)
        {
            GameObject reuse = m_pool.Dequeue();
            reuse.transform.position = position;
            reuse.transform.rotation = rotation;
            reuse.SetActive(true);
            return reuse;
        }
        else if (m_pool.Count <= 0)
        {
            GameObject go = Instantiate(prefab) as GameObject;
            go.transform.position = position;
            go.transform.rotation = rotation;
            return go;
        }
        else
        {
            return null;
        }
    }


    public void Recovery(GameObject recovery)
    {
        m_pool.Enqueue(recovery);
        recovery.SetActive(false);
    }
}
