﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

public class Shield : MonoBehaviour
{
   
    public FightSystem fightSystem;
    public float weapondamage;
    public float maxPower;
    public float currentPower;
    public bool takedamage;
    public Collider col;
    //public  bool deleate 
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider>();
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
        weapondamage = fightSystem.damage;
        currentPower = maxPower;
        print(col.name);
    }

    // Update is called once per frame
    void Update()
    {
        if(currentPower <= 0)
        {
            currentPower = 0;
            Destroy(gameObject, 1f);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Weapon_point")
        {
            print(other.gameObject.name);
            currentPower -= weapondamage;
            print(currentPower);
            takedamage = true;
        }
            

    }
}
