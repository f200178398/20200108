﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
using TMPro;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
public class UIManager : MonoBehaviour
{
    [Header("Controller UI")]
    public FightSystem fightSystem;
    public Image hpBar;
    public Image hpBarBackGround;
    public GameObject controller;
    public Canvas controllerBar;
    public float c_CurrentHp;

    [Header("Boss UI")]
    public AI_data bossData;
    public GameObject boss;
    public Canvas bossHpCanvas;
    public Image BossHpBar;
    public float BossCurrentHp;
    public float fillamount;
    float lerpSpeed = 0.2f;
    float lerpspeedSlow = 0.1f;

    [Header("Chat UI")] //腳色對話窗的背景
    public bool chatBGswitch;
    public Image Chat_BG;
    public Canvas chatBG;

    public bool isDialog;
    public TextMeshProUGUI textDisplay;
    public string[] sentences_01;
    public string[] sentences_02;
    private int index_01;
    private int index_02;
    public float typingSpeed;
    public GameObject continueButton;
    public Animator textAnim;

    [Header("觸發對話的區域")]
    public Collider d_tri_01;
    public Dialog_Tri dialog_Tri;
    [Header("動畫觸發條件")]
    public TimelineManager BossDeadTimeLine;
    public TimelineManager zoneTwoStartTimeLine;
    public TimelineManager StartExplosionTimeline;

    [Header("任務")]
    public NiceKyleBattleMission niceKyleBattleMission;

    public bool closeBossDeadCoroutine = false;
    public bool closeBatteryEndCoroutine = false;
    [Header("WakizaShi 增加攻擊力")]
    public Image wakizashiBar;
    public float wakizashiTime;
    [Header("聲音")]
    public AudioListener mainCameraListener;



    void Start()
    {
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
        hpBar = GameObject.Find("C_CurrentHP").GetComponent<Image>();
        hpBarBackGround = GameObject.Find("C_Hp_BG").GetComponent<Image>();
        controller = GameObject.Find("Controller");
        controllerBar = GameObject.Find("ControllerBar").GetComponent<Canvas>();
        bossHpCanvas = GameObject.Find("BossHpBar").gameObject.GetComponent<Canvas>();
        boss = GameObject.Find("Boss");
        bossData = boss.gameObject.GetComponent<AI_data>();
        BossHpBar = GameObject.Find("BossCurrentHP").GetComponent<Image>();
        bossHpCanvas.gameObject.SetActive(false);
        wakizashiBar.gameObject.SetActive(false);
        //BossDeadUI = GameObject.Find("BossDeadUI").GetComponent<Canvas>();
        //chatBG = GameObject.Find("Chat_BG_ALL").GetComponent<Canvas>();

        //dialog_Tri = GameObject.Find("d_tri_01").GetComponent<Dialog_Tri>();

        if (!gameObject.activeInHierarchy && gameObject.tag == ("Enemy"))
        {
            print(gameObject.name);
        }
        //StartCoroutine(Type());
        BossDeadTimeLine = GameObject.FindGameObjectWithTag("BossDeadTimeLine").GetComponent<TimelineManager>();
        zoneTwoStartTimeLine = GameObject.FindGameObjectWithTag("Zone_2_Start").GetComponent<TimelineManager>();
        niceKyleBattleMission = GameObject.FindGameObjectWithTag("HeavenRoadTri").GetComponent<NiceKyleBattleMission>();
        StartExplosionTimeline = GameObject.FindGameObjectWithTag("StartExplosionTimeline").GetComponent<TimelineManager>();
        //cv_PlayerDeadUI = GameObject.Find("PlayerDeadUICanvas").GetComponent<Canvas>();
        //im_GameOverLogo = GameObject.Find("GameOver").GetComponent<Image>();
        //bt_DeadMessage = GameObject.Find("DeadMessage_Button").GetComponent<Button>();
        //bt_QuitGameMessage = GameObject.Find("Button").GetComponent<Button>();
        //tmp_DeadMessage = GameObject.Find("DeadMessage_Button").GetComponentInChildren<TextMeshProUGUI>();
        //tmp_QuitGameMessage = GameObject.Find("QuitGameMessage_Button").GetComponentInChildren<TextMeshProUGUI>();

        mainCameraListener = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioListener>();

        StartCoroutine(LoadingCanvasDisappear());

        //鎖住滑鼠，和隱藏滑鼠
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Contuine();
        StartCoroutine(ControllerHp());
        StartCoroutine(BossHp());
        //CloseChatBG();


        //if(dialog_Tri.d_tri == true)
        //{
        //    print("開始對話");
        //}
        StartShowPlayerDeadUI();
        
        StartCoroutine(BossDead());

        ShowGameMenu();
        //OptionMenu_CheckAudioListenForButtonColor();

        SetbatteryUI();
        //BossDead();
        StartCoroutine(batteryMissionEnd());
        GetWakizashiTime();


        TimelinePlayingNoShowHPAndMinimap();
    }


    IEnumerator ControllerHp() //這裡用來控制 角色HP
    {
        c_CurrentHp = fightSystem.currentHp;
        float chp = fightSystem.GetHealthRate();
        hpBar.fillAmount = Mathf.Lerp(hpBar.fillAmount, chp, lerpSpeed);

        yield return new WaitForSeconds(3f);
        hpBarBackGround.fillAmount = Mathf.Lerp(hpBar.fillAmount, chp, lerpspeedSlow);


    }

    IEnumerator BossHp()
    {
        float hp = bossData.GetHealthRate();
        BossHpBar.fillAmount = Mathf.Lerp(BossHpBar.fillAmount, hp, lerpSpeed);
        float dis = Vector3.Distance(controller.transform.position, boss.transform.position);
        if (dis < 30f && bossData.enemyTarget.currentState != SA.StateType.LastBossDeath)
        {
            bossHpCanvas.gameObject.SetActive(true);
        }
        else if (hp <= 0)
        {
            yield return new WaitForSeconds(3);
            bossHpCanvas.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// 對話視窗背景
    /// </summary>
    public void CloseChatBG()
    {
        if (isDialog == true) //如果對話視窗開始
        {
            //hpBar.canvasRenderer.cull = true;
            chatBG.gameObject.SetActive(true);  //開啟對話的背景 (灰色透明)

            controllerBar.gameObject.SetActive(false); //並關掉玩家UI
        }
        else
        {
            controllerBar.gameObject.SetActive(true); //開啟玩家UI
                                                      //hpBar.canvasRenderer.cull = false;

            chatBG.gameObject.SetActive(false); //關掉對話背景
        }
    }

    public IEnumerator Type()
    {
        isDialog = true;

        //if(chatBGswitch == true)
        //{
        foreach (char letter in sentences_01[index_01].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        //}

    }
    public IEnumerator Type_02()
    {
        foreach (char letter in sentences_02[index_02].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public IEnumerator CloseBG()
    {
        yield return StartCoroutine(Type());
        chatBG.gameObject.SetActive(false);
    }

    public void NextSentence() //到下一句對話內容
    {
        textAnim.SetTrigger("Change");
        continueButton.SetActive(false);
        if (index_01 < sentences_01.Length - 1)
        {
            index_01++;
            textDisplay.text = "";
            StartCoroutine(Type());
            //return;
        }
        else
        {
            textDisplay.text = "";
            continueButton.SetActive(false);
        }
        // if (index_02 < sentences_02.Length - 1)
        //{
        //    index_02++;
        //    textDisplay.text = "";
        //    StartCoroutine(Type_02());
        //}
        //else
        //{
        //    textDisplay.text = "";
        //    continueButton.SetActive(false);
        //}
    }

    public void Contuine() //按下BUTTON後繼續下一段對話
    {
        if (textDisplay.text == sentences_01[index_01])
        {
            continueButton.SetActive(true);
        }
        else if (textDisplay.text == sentences_02[index_02])
        {
            continueButton.SetActive(true);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        //dialog_Tri.OnTriggerEnter(other);
    }

    #region 顯示玩家死掉時的UI
    public delegate void PlayerDead();
    public event PlayerDead showGameOverUI;

    [Header("玩家死亡畫面UI")]
    public Canvas cv_PlayerDeadUI;
    public Image im_GameOverLogo;
    public Button bt_DeadMessage;
    public Button bt_QuitGameMessage;

    public void StartShowPlayerDeadUI()
    {

        //if (Input.GetKey(KeyCode.Escape))
        //{

        //    StartCoroutine(_QuitGame(0.5f));
        //}
        if (fightSystem.currentHp <= 0)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            StartCoroutine(_ShowPlayerDeadUI(0.5f));

        }
        else if (fightSystem.currentHp > 0)
        {
            
            cv_PlayerDeadUI.gameObject.SetActive(false);
            a_GameOverLogo = 0;
            a_DeadMessage = 0;
            a_DeadMessageText = 0;
            a_QuitGameMessage = 0;
            a_QuitGameMessageText = 0;
        }

    }
    [SerializeField]
    private float a_GameOverLogo;
    [SerializeField] private float a_DeadMessage;
    [SerializeField] private float a_DeadMessageText;
    [SerializeField] private float a_QuitGameMessage;
    [SerializeField] private float a_QuitGameMessageText;

    public TextMeshProUGUI tmp_DeadMessage;
    public TextMeshProUGUI tmp_QuitGameMessage;

    public IEnumerator _ShowPlayerDeadUI(float waitSeconds)
    {
        //等幾秒，開畫布(目前沒東西)
        yield return new WaitForSeconds(waitSeconds);
        cv_PlayerDeadUI.gameObject.SetActive(true);

        //等幾秒，開GameOver字樣
        yield return new WaitForSeconds(1.0f);
        im_GameOverLogo.gameObject.SetActive(true);
        a_GameOverLogo += Time.deltaTime;
        im_GameOverLogo.color = new Color(1, 1, 1, a_GameOverLogo);

        //等幾秒，開死亡訊息
        yield return new WaitForSeconds(1.0f);
        bt_DeadMessage.gameObject.SetActive(true);
        a_DeadMessage += Time.deltaTime;
        a_DeadMessageText += Time.deltaTime;
        if (a_DeadMessage > 183f / 255f) { a_DeadMessage = 183f / 255f; }
        bt_DeadMessage.image.color = new Color(183f / 255f, 183f / 255f, 183f / 255f, a_DeadMessage);
        tmp_DeadMessage.color = new Color(140f / 255f, 0, 0, a_DeadMessageText);

        //等幾秒，開離開遊戲訊息
        yield return new WaitForSeconds(1.0f);
        bt_QuitGameMessage.gameObject.SetActive(true);
        a_QuitGameMessage += Time.deltaTime;
        a_QuitGameMessageText += Time.deltaTime;
        if (a_QuitGameMessage > 205f / 255f) { a_QuitGameMessage = 205f / 255f; }
        bt_QuitGameMessage.image.color = new Color(170f / 255f, 170f / 255f, 170f / 255f, a_QuitGameMessage);
        tmp_QuitGameMessage.color = new Color(50f / 255f, 50f / 255f, 50f / 255f, a_QuitGameMessage);

    }

    public void PlayerDeadUI_QuitGame()
    {
        Debug.Log("Button Clicked");
        StartCoroutine(_QuitGame(0.5f));
    }
    public IEnumerator _QuitGame(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        Debug.Log("Quit Game Worked");
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();

    }

    public void PlayerDeadUI_Respawn()
    {
        fightSystem.b_couldRespawn = true;
        //cv_PlayerDeadUI.gameObject.SetActive(false);
        //a_GameOverLogo = 0;
        //a_DeadMessage = 0;
        //a_DeadMessageText = 0;
        //a_QuitGameMessage = 0;
        //a_QuitGameMessageText = 0;
    }
    #endregion
    public Canvas BossDeadUI;


    #region 按Esc之後出現的選單
    [Header("遊戲選單")]
    public GameObject go_GameMenu;
    public Canvas cv_GameMenu;
    public Button bt_GameCountinue;
    public Button bt_GameQuit;
    public Button bt_BackToMenu;
    public Button bt_Option;
    public string s_goToScene = null;
    [Tooltip("下面這邊開始是遊戲選項裡面的東西")]
    public GameObject go_OptionMenu;
    public Button bt_BGMTurnOn;
    public Button bt_BGMTurnOff;
    public Button bt_EffectSoundTurnOn;
    public Button bt_EffectSoundTurnOff;
    public Button bt_BackToGameMenu;
    public GameObject go_AuthorMenu;

    /// <summary>
    /// //如果用搖桿的話，叫出選單的按鈕放在這邊 
    /// 會一直檢查有沒有按下Esc鍵叫出GameMenu的函式 
    /// </summary>
    public void ShowGameMenu()
    {
        OptionMenu_CheckAudioListenForButtonColor();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
           
            cv_GameMenu.enabled = !cv_GameMenu.enabled; //指派一個與現在enable相反的enable給他

            if (go_OptionMenu.activeInHierarchy) { go_OptionMenu.SetActive(false); }
            if (go_AuthorMenu.activeInHierarchy) { go_AuthorMenu.SetActive(false); }
        }
        if (cv_GameMenu.enabled)
        {
            Time.timeScale = 0f;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1f;
            if (fightSystem.currentHp >0 && bossData.enemyTarget.currentState != SA.StateType.LastBossDeath)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

        }
    }

    /// <summary>
    /// GameMenu的繼續遊戲按鈕被按下的話
    /// </summary>
    public void GameMenu_GameContinueButtonClicked()
    {
        cv_GameMenu.enabled = false;
    }
    /// <summary>
    /// GameMenu的離開遊戲按鈕被按下的話
    /// </summary>
    public void GameMenu_GameQuitButtonClicked()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
    /// <summary>
    /// GameMenu的回主畫面按鈕被按下的話
    /// </summary>
    public void GameMenu_BackToStartMenu()
    {
        SceneManager.LoadScene(s_goToScene);
    }
    /// <summary>
    /// GameMenu的遊戲設定按鈕被按下的話
    /// </summary>
    public void GameMenu_OptionButtonClicked()
    {
        //打開子選單
        if (!go_OptionMenu.activeInHierarchy)
        {
            go_OptionMenu.SetActive(true);
        }
        else
        {
            go_OptionMenu.SetActive(false);
        }
        //關掉作者頁面，一定只能開啟設定頁面就對了
        if (go_AuthorMenu.activeInHierarchy)
        {
            go_AuthorMenu.SetActive(false);
        }
    }
    /// <summary>
    /// 設定頁面的製作名單按鈕被按下的話
    /// </summary>
    public void OptionMenu_ShowAuthorMenuButtonClicked()
    {
        if (!go_AuthorMenu.activeInHierarchy) { go_AuthorMenu.SetActive(true); }
        if (go_OptionMenu.activeInHierarchy) { go_OptionMenu.SetActive(false); }

    }
    /// <summary>
    /// 製作者頁面的返回按鈕被按下的話
    /// </summary>
    public void AuthorMenu_BackToOptionMenuButtonClicked()
    {
        if (go_AuthorMenu.activeInHierarchy) { go_AuthorMenu.SetActive(false); }
        if (!go_OptionMenu.activeInHierarchy) { go_OptionMenu.SetActive(true); }
    }

    //下面四個是按下On/Off按鈕的時候要切換音樂音效用的
    public void OptionMenu_BGMOnButtonClicked()
    {
        bt_BGMTurnOn.image.color = new Color(255f / 255f, 225f / 255f, 138f / 255f, 1);
        bt_BGMTurnOff.image.color = new Color(1, 1, 1, 1);
        //mainCameraListener.enabled = true;
        AudioListener.volume = 1.0f;
    }
    public void OptionMenu_BGMOffButtonClicked()
    {
        bt_BGMTurnOff.image.color = new Color(255f / 255f, 225f / 255f, 138f / 255f, 1);
        bt_BGMTurnOn.image.color = new Color(1, 1, 1, 1);
        //mainCameraListener.enabled = false;
        AudioListener.volume = 0.0f;            
    }
    public void OptionMenu_EffectSoundOnButtonClicked()
    {
        bt_EffectSoundTurnOn.image.color = new Color(255f / 255f, 225f / 255f, 138f / 255f, 1);
        bt_EffectSoundTurnOff.image.color = new Color(1, 1, 1, 1);
    }
    public void OptionMenu_EffectSoundOffButtonClicked()
    {
        bt_EffectSoundTurnOff.image.color = new Color(255f / 255f, 225f / 255f, 138f / 255f, 1);
        bt_EffectSoundTurnOn.image.color = new Color(1, 1, 1, 1);
    }

    //這個是寫來檢查音樂音效狀態，來確保按鈕顏色的(備用)
    public void OptionMenu_CheckAudioListenForButtonColor()
    {
        if (AudioListener.volume >=0.9f)
        {
            bt_BGMTurnOn.image.color = new Color(255f / 255f, 225f / 255f, 138f / 255f, 1);
            bt_BGMTurnOff.image.color = new Color(1, 1, 1, 1);
        }
        else if(AudioListener.volume <=0.1f)
        {
            bt_BGMTurnOff.image.color = new Color(255f / 255f, 225f / 255f, 138f / 255f, 1);
            bt_BGMTurnOn.image.color = new Color(1, 1, 1, 1);
        }
    }

    #endregion

    #region 開啟BOSS死亡動畫 並 在動畫結束之後顯示UI
    public IEnumerator BossDead()
    {
        if (bossData.enemyTarget.currentState == SA.StateType.LastBossDeath)
        {
            
            if (!closeBossDeadCoroutine)
            {
                BossDeadTimeLine.timelineTrigger = true;
                BossDeadTimeLine.TimelineAnimationStart();
                closeBossDeadCoroutine = true;

            }
            else if (BossDeadTimeLine.timelineAnimation.state==PlayState.Paused)
            {
                
                yield return new WaitForSeconds(2);     
                BossDeadUI.gameObject.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
        else
        {
            BossDeadUI.gameObject.SetActive(false);
        }
    }
    #endregion

    public void GetWakizashiTime()
    {
        float _lerptime = 1f;
        float time = fightSystem.WakizashiTime();
        if (fightSystem.onTrigger.isGetWakizashi)
        {
            wakizashiBar.gameObject.SetActive(true);//碰到物品後開啟UI
            
            fightSystem.wakizashiTime -= Time.deltaTime * 1f;
            wakizashiBar.fillAmount = Mathf.Lerp(wakizashiBar.fillAmount, time, _lerptime);
        }
        else
        {
            wakizashiBar.gameObject.SetActive(false);
        }
       
    }

    #region 上層平台的蒐集核心任務
    [Header("蒐集核心的UI")]
    public Canvas batteryUI;
    public Text battery;

    public void SetbatteryUI()
    {
        if (niceKyleBattleMission.i_WatchGetTreasure < niceKyleBattleMission.i_needToGetHowManyTreasure)
        {
            battery.text = niceKyleBattleMission.i_WatchGetTreasure.ToString() + " / " + niceKyleBattleMission.i_needToGetHowManyTreasure;
        }
        else if (niceKyleBattleMission.i_WatchGetTreasure == niceKyleBattleMission.i_needToGetHowManyTreasure)
        {
            battery.text = niceKyleBattleMission.i_needToGetHowManyTreasure.ToString() + " / " + niceKyleBattleMission.i_needToGetHowManyTreasure;
        }
    }

    public IEnumerator batteryMissionEnd()
    {
        if (niceKyleBattleMission.bStartExplosion == true && !closeBatteryEndCoroutine)
        {


            StartExplosionTimeline.timelineTrigger = true;
            //StartExplosionTimeline.TimelineAnimationStart();
            closeBatteryEndCoroutine = true;

            yield return null;

        }
    }
    #endregion

    #region 擋住loadScene的畫布
    [Header("擋住loadScene的畫布")]
    public GameObject go_loadingCanvas;
    public IEnumerator LoadingCanvasDisappear()
    {
        yield return new WaitForSeconds(0.5f);
        go_loadingCanvas.SetActive(false);
    }
    #endregion

    #region 各種動畫播放時，小地圖和血條消失
    [Header("切換攝影機的動畫，小地圖和血條消失")]
    public GameObject go_controllerHP;
    public GameObject go_Minimap;

    //各個會切攝影機的timeline
    public TimelineManager tm_kyleBeBullied;
    public TimelineManager tm_kyleSpeak;
    public TimelineManager tm_niceKyleBattle;
    public TimelineManager tm_startExplosion;
    public TimelineManager tm_AirShipAppeared;
    public TimelineManager tm_BossAppeared;
    public TimelineManager tm_BossDead;

    public void TimelinePlayingNoShowHPAndMinimap()
    {
        if (tm_kyleBeBullied.timelineAnimation.state == PlayState.Playing ||
            tm_kyleSpeak.timelineAnimation.state == PlayState.Playing ||
            tm_niceKyleBattle.timelineAnimation.state ==PlayState.Playing ||
            tm_startExplosion.timelineAnimation.state == PlayState.Playing ||
            tm_AirShipAppeared.timelineAnimation.state == PlayState.Playing ||
            tm_BossAppeared.timelineAnimation.state == PlayState.Playing ||
            tm_BossDead.timelineAnimation.state == PlayState.Playing ||
            bossData.enemyTarget.currentState == SA.StateType.LastBossDeath
            )
        {
            go_controllerHP.SetActive(false);
            go_Minimap.SetActive(false);
        }
        else
        {
            go_controllerHP.SetActive(true);
            go_Minimap.SetActive(true);
        }
    }

    #endregion
}
