﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class Helper : MonoBehaviour
    {
        [Range(-1, 1f)]
        public float vertical; //宣告個前進速度,範圍0~1
        [Range(-1, 1f)]
        public float horizontal;
        public bool playAnim;  //宣告一個播放動作的bool
        public bool twoHanded; //宣告一個用來切換攻擊姿態的bool
        public bool create;
        public bool interacting;
        public bool lockOn;

        public string[] AttackType_A;
        public string[] AttackType_B;

        public bool enableRootMotion;

        Animator anim;//動畫控制器的變數
        public bool TwoHand { get => twoHanded; set => twoHanded = value; }
        // Start is called before the first frame update
        void Start()
        {
            anim = GetComponent<Animator>(); //先調用Amiator元件
            anim.applyRootMotion = false; //Animator的applyRootMotion 在開始時為假
        }

        // Update is called once per frame
        void Update()
        {
            enableRootMotion = !anim.GetBool("canMove");
            anim.applyRootMotion = enableRootMotion;

            interacting = anim.GetBool("interacting");

            if (lockOn == false)
            {
                horizontal = 0;
                vertical = Mathf.Clamp01(vertical); //最大為-1 ~ 1
            }
            anim.SetBool("lock on", lockOn);

            if (enableRootMotion)
                return;

            if (create)
            {
                anim.Play("create");
                create = false;
            }
            if (interacting)
            {
                playAnim = false;
                vertical = Mathf.Clamp(vertical, 0, 0.5f);
            }

            anim.SetBool("two_handed", twoHanded); //撥放

            if (playAnim) //如果播放動畫
            {
                string targetAnim;
                if (!twoHanded)
                {
                    int r = Random.Range(0, AttackType_B.Length);
                    targetAnim = AttackType_B[r];
                    if (vertical > 0.8f)
                    {
                        targetAnim = "Run_Light_Attk_1";
                    }
                }
                else
                {
                    int r = Random.Range(0, AttackType_A.Length);
                    targetAnim = AttackType_A[r];
                    if (vertical > 0.8f)
                    {
                        targetAnim = "Run_Light_Attk_2";
                    }

                }
                vertical = 0;

                anim.CrossFade(targetAnim, 0.2f);//播放的動畫會在2秒後完成
                anim.SetBool("canMove", false);
                enableRootMotion = true;
                playAnim = false;

            }
           // anim.SetFloat("vertical", vertical);
           // anim.SetFloat("horizontal", horizontal);

        }
    }
}
