﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class StateManager : MonoBehaviour
    {
        public static StateManager m_instance;
        public static StateManager Instance() { return m_instance; }

        public StateManager()
        {
            m_instance = this;
        }
        [Header("Init")]
        public GameObject activeModel;

        public GameObject[] weapeonChange;
        public GameObject[] weaponVFX;

        [Header("Inputs")]
        public float vertical;
        public float horizontal;
        public float jump;
        public float moveAmount;
        public float turnMoveAmount;
        public Vector3 moveDir;
        public bool rt, rb, lt, lb, Y, jum, A;
        //public bool twoHanded;

        [Header("Stats")]
        public float moveSpeed = 2;
        public float runSpeed = 3.5f;
        public float rotateSpeed = 5f;
        public float toGround = 0.5f;



        [Header("States")]
        public bool onGround;
        public bool run;
        public bool lockOn;
        public bool inAction;
        public bool canMove;
        public bool slide;//y
        public bool jumping;
        public bool isTwoHanded;
        public bool isTiming;
        public bool activeWeapon;
        public bool activeWeaponVFX;
        public bool isAttack;
        public bool underAttack;

        [Header("Enemy")]
        public EnemyTarget lockOnTarget;
        public Transform Etarget;


        //public AnimatorStateInfo animInfo;
        public Collider col;
        [HideInInspector]
        public Animator anim;
        [HideInInspector]
        public Rigidbody rigid;
        [HideInInspector]
        public AnimatorHook a_hook;
        [HideInInspector]
        public FightSystem fightSystem;
        [HideInInspector]
        public OnTrigger onTrigger;
        [HideInInspector]
        public float delta;
        [HideInInspector]
        public LayerMask ignoreLayers;
        [HideInInspector]
        public int attack_count;

        [HideInInspector]
        public AnimatorStateInfo animInfo;
        public float currentTime;
        public float countTime; //未使用

        public Collider weapon_col;


        public float TemP_angle;



        float _actionDelay;

        public string[] AttackType_A; //搖桿4  按鍵 R的攻擊招式
        public string[] AttackType_B; //搖桿1 按鍵 C的攻擊招式
        public string[] AttackType_C; //搖桿3 按鍵  的踢技
        public string[] AttackType_D; //搖桿3 的位移技能 
        public string[] AttackType_E; //空中的攻擊列 (不在地面上時的攻擊模式)

        public void Init()
        {
            StepAnimator();
            rigid = GetComponent<Rigidbody>();
            rigid.angularDrag = 999;
            rigid.drag = 4;
            rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | 
                RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
            col = GetComponent<Collider>();
            a_hook = activeModel.AddComponent<AnimatorHook>();
            a_hook.Init(this);

            gameObject.layer = 8; //將主角設為圖層8
            ignoreLayers = ~(0 << 9);
            //lockOn = true;
            anim.SetBool("onGround", true);

            //weapeon = GameObject.Find("Weapon_point (1)");

            AnimatorStateInfo animInfo = anim.GetCurrentAnimatorStateInfo(0);


            weapon_col = weapeonChange[0].GetComponent<Collider>();// 先獲得第一把武器的碰撞, 並在CanMove 時關閉
                                                                   //print(weapon_col); 

            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
            onTrigger = GetComponent<OnTrigger>();

        }

        void StepAnimator()
        {


            if (activeModel == null)
            {
                anim = GetComponent<Animator>();
                if (anim == null)
                {
                    Debug.Log("no model found");
                }
                else
                {
                    activeModel = anim.gameObject;
                }
            }
            if (anim == null)
                anim = GetComponent<Animator>();

            anim.applyRootMotion = false;
        }
        public void FixedTick(float d)
        {
            delta = d;

            DetectAction();

            if (inAction)
            {
                anim.applyRootMotion = true;

                _actionDelay += delta;
                if (_actionDelay > 0.3f)
                {
                    inAction = false;
                    _actionDelay = 0;
                }
                else
                {
                    return;
                }

            }


            canMove = anim.GetBool("canMove");

            if (!canMove)
                return;

            anim.applyRootMotion = false;

            rigid.drag = (moveAmount > 0 || onGround == false) ? 0 : 4;

            float targetSpeed = moveSpeed;

            if (canMove)
                weaponVFX[0].SetActive(false); //走路時關閉武器特效
            weapon_col.enabled = !enabled; // 走路時關閉武器碰撞

            if (run)
            {

                targetSpeed = runSpeed;
                lockOn = false;
                activeWeapon = false;
                weapeonChange[0].SetActive(false);
            }

            //if (onTrigger.underAttack == true)
            //{
            //    anim.SetTrigger("take damageTri");

            //    print("受傷動畫持續");
            //}
            ////else
            ////{
            ////    anim.SetBool("take damage", false);
            ////}

            if (onGround)
                rigid.velocity = moveDir * (targetSpeed * moveAmount);
            if (run)
                lockOn = false;
            if (onGround == false) //y 未完成, 讓腳色奔跑時往下跳 能切換到onground
            {
                rigid.constraints = RigidbodyConstraints.None;
                rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
                run = false;
            }

            anim.SetBool("onGround", true);

            if (onGround)
                ToJump();

            if (onTrigger.isFallDown)
            {
                moveSpeed = 0;
                runSpeed = 0;
                rotateSpeed = 0;
                onTrigger.isFallDown = false;
            }
            //print("傳到狀態機的值: " +onTrigger.underAttack);




            Vector3 targetDir = (lockOn == false) ? moveDir
             : lockOnTarget.transform.position - transform.position;


            targetDir.y = 0;
            if (targetDir == Vector3.zero)
                targetDir = transform.forward;
            Quaternion tr = Quaternion.LookRotation(targetDir);
            Quaternion targetRotation = Quaternion.Slerp(transform.rotation, tr, delta * moveAmount * rotateSpeed);
            transform.rotation = targetRotation;



            anim.SetBool("lock on", lockOn);

            TemP_angle = Quaternion.Angle(transform.rotation, tr);


            if (lockOn == false)
                HandleMovementAnimations();
            else
                HandleLockOnAnimations(moveDir);
            //print(lockOn);

            DeadAnim();
        }

        public void DetectAction()
        {
            CountCurrentTime();

            if (canMove == false)
                return;

            if (rb == false && rt == false && lb == false && lt == false)
                return;

            string targetAnim = null;

            if (currentTime > 2)  //用計時器重置攻擊動作,目前並非好方法, 攻擊動作陣列數太長的話 來不及打完
            {
                attack_count = 0;

                print("攻擊動畫已經重置");
            }

            if (rb)
            {
                //print("rb");




                //targetAnim = "two_handed_idle";
            }
            if (rt) //鍵盤連擊  //搖桿4號鍵 
            {

                print("4");//搖桿按鍵號
                ActiveWeapeon(); // 顯示武器
                ActiveWeapeonVFX(); //顯示武器特效
                StartNewTime();//開始計時

                if (attack_count < AttackType_A.Length)
                {
                    targetAnim = AttackType_A[attack_count];

                    attack_count++;
                    if (jum && fightSystem.Power() > 80f)
                    {
                        fightSystem.currentPower -= 80f;
                        targetAnim = "迴旋踢";
                    }

                }

                else
                {
                    attack_count = 0;

                }

                if (run && fightSystem.Power() >= 99f)
                {
                    fightSystem.currentPower -= 99f;
                    targetAnim = "R_Attack_07";
                }
                //if (weaponManager.attack == true)                    
                //    isAttack = true; 
            }


            //targetAnim = "Light_Attk_2";
            if (lb)// 這裡用來翻滾或是位移閃避動作  並且可以使用踢技
            {
                StartNewTime();
                print("leftDown");

                Roll();

                if (attack_count < AttackType_D.Length)
                {
                    targetAnim = AttackType_D[attack_count];
                    attack_count++;
                }
                else
                {
                    attack_count = 0;
                }


                if (jum)
                {
                    if (attack_count < AttackType_C.Length)
                    {
                        targetAnim = AttackType_C[attack_count];
                        // Debug.Log("LT");
                        attack_count++;
                        //if (rt)
                        //{
                        //    targetAnim = "旋風斬";
                        //}
                    }
                    else
                    {
                        attack_count = 0;
                    }
                    //targetAnim = "迴旋踢";
                    //ColTransform();
                }

                if (run && fightSystem.Power() > 80f)
                {
                    fightSystem.currentPower -= 80f;
                    targetAnim = "滑行昇龍拳";
                }

                if (!onGround) //(COL)不在地面上時會使用下列攻擊
                {
                    if (attack_count < AttackType_E.Length)
                    {
                        targetAnim = AttackType_E[attack_count];
                    }

                }
            }

            //HandleTwoWeapon();                

            if (lt) //搖桿1號鍵 
            {
                print("1");
                ActiveWeapeon();
                ActiveWeapeonVFX();
                StartNewTime();
                if (attack_count < AttackType_B.Length)
                {
                    targetAnim = AttackType_B[attack_count];
                    // Debug.Log("LT");
                    attack_count++;
                    if (rt && fightSystem.Power() >= 90f)
                    {
                        fightSystem.currentPower -= 90f;
                        targetAnim = "旋風斬";
                    }
                }
                else
                {
                    attack_count = 0;
                }

                //targetAnim = AttackType_B[rB];
                if (run && fightSystem.Power() >= 80f)
                {
                    fightSystem.currentPower -= 80f;
                    targetAnim = "刺擊";
                }
            }

            if (jum)
            {
                if (rt)
                {

                }
            }

            if (run)
            {
                if (jum && fightSystem.Power() > 80f)
                {
                    fightSystem.currentPower -= 80f;
                    targetAnim = "迴旋踢";
                }
            }


            if (string.IsNullOrEmpty(targetAnim))  //IsNullOrEmpty
                return;

            canMove = false;
            inAction = true;
            anim.CrossFade(targetAnim, 0.1f);
            //rigid.velocity = Vector3.zero;

        }

        public void Tick(float d)
        {
            delta = d;
            onGround = OnGround();
            anim.SetBool("onGround", onGround);
            //Debug.Log(onGround);
        }

        void HandleMovementAnimations()
        {
            anim.SetBool("run", run);
            anim.SetFloat("Turn", turnMoveAmount, 0.1f, Time.deltaTime);
            anim.SetFloat("vertical_1", moveAmount, 0.1f, Time.deltaTime);
            //anim.SetFloat("vertical_1", moveAmount);
            //anim.SetFloat("FloatTest",moveAmount);


        }

        void HandleLockOnAnimations(Vector3 moveDir)
        {
            Vector3 relativeDir = transform.InverseTransformDirection(moveDir);
            float h = relativeDir.x;
            float v = relativeDir.z;

            anim.SetFloat("vertical_1", v, 0.1f, delta);
            anim.SetFloat("horizontal_1", h, 0.1f, delta);
        }

        public bool OnGround()
        {
            bool r = false;
            Vector3 origin = transform.position + (Vector3.up * toGround);
            Vector3 dir = -Vector3.up;
            float dis = toGround + 0.3f;
            RaycastHit hit;
            Debug.DrawRay(origin, dir * dis, Color.green);
            if (Physics.Raycast(origin, dir, out hit, dis, ignoreLayers))
            {
                r = true;
                Vector3 targetPosition = hit.point;
                transform.position = targetPosition;
                //Debug.Log("work");
            }

            return r;

        }
        public void HandleTwoWeapon()
        {
            anim.SetBool("two_handed_idle", isTwoHanded);
            print("TwoHand");
        }

        //public void AttackTrigger()
        //{
        //    weaponManager.attack = true;
        //    isAttack = true;

        //}
        //public void OnTriggerEnter(Collider col)
        //{
        //    if (col.gameObject.tag == "Enemy")
        //        isAttack = true;

        //    print("ATTACK");
        //}
        //public void OnTriggerExit(Collider col)
        //{
        //    if (col.gameObject.tag == "Enemy")
        //        isAttack = false;
        //}


        //public void Wrap()//閃現攻擊
        //{
        //    transform.localPosition = Vector3.MoveTowards(transform.localPosition, Etarget.position , 100*Time.deltaTime);

        //}
        public void StartNewTime() //開始計時, 時間為0
        {
            isTiming = true;
            currentTime = 0;
        }
        public void CountCurrentTime()//計時器開始
        {
            if (isTiming)
            {
                currentTime += Time.fixedDeltaTime;
            }
            if (currentTime > 10)
            {
                weapeonChange[0].SetActive(false);
                weapeonChange[1].SetActive(true);
            }
            if (currentTime > 2)
            {
                weaponVFX[0].SetActive(false);
            }

        }
        public void AnimatorInfo()
        {
            if (animInfo.normalizedTime < 1.0f)
            {

                //print("AAAAAAAAAAAAAA");

            }
        }

        public void DeadAnim()
        {
            if (fightSystem.currentHp <= 0)
            {
                //anim.SetTrigger("dead");
                anim.SetBool("Dead",true);
            }
        }
        public void ToJump()
        {
            if (jum)
            {
                jumping = true;
                Debug.Log("jumpping");
                anim.SetBool("jump_1", true);
                jumping = false;
            }
            else
            {
                jumping = false;
                anim.SetBool("jump_1", false);
            }

        }
        public void UnderAttack() //受到攻擊
        {

            if (onTrigger.underAttack)
            {
                print("ooo");
            }

        }

        public void ActiveWeapeon()
        {

            weapeonChange[0].SetActive(true);
            weapeonChange[1].SetActive(false);
            activeWeapon = true;

        }
        public void ActiveWeapeonVFX()
        {
            weaponVFX[0].SetActive(true);
            activeWeaponVFX = true;
        }

        public void Roll() //翻滾
        {
            Vector3 endpos = transform.position + transform.forward * 8f; //往前 15f
            transform.position = Vector3.Lerp(transform.position, endpos, 2f * Time.deltaTime);  //往前瞬間移動
        }

        public void Warp() //突進
        {
            Vector3 endpos = transform.position + transform.forward * 30f; //往前 15f
            transform.position = Vector3.Lerp(transform.position, endpos, 5f * Time.deltaTime);  //往前瞬間移動
        }

        public void WarpKick()
        {
            Vector3 endpos = transform.position + transform.forward * 10f; //往前 15f
            transform.position = Vector3.Lerp(transform.position, endpos, 1f * Time.deltaTime);  //往前瞬間移動
            //col.transform.position = Vector3.Lerp(col.transform.position, endpos, 2f * Time.deltaTime);
        }

        public void OpenUchanCol()
        {

            col.enabled = enabled;
        }
        public void CloseUchanCol()
        {

            col.enabled = !enabled;
        }
        public void ColTransform()
        {
            col.transform.position = this.transform.position;
        }

        #region 配合Timeline播放的速度
        /// <summary>
        /// 一般情況下的各種速度
        /// </summary>
        public void EverySpeedInitSettings()
        {
            moveSpeed = 7;
            runSpeed = 10f;
            rotateSpeed = 8f;
            toGround = 0.5f;
        }
        /// <summary>
        /// Timelin播放時的速度設定 
        /// </summary>
        public void TimelinePlayingSpeedSettings()
        {
            moveSpeed = 0;
            runSpeed = 0;
            rotateSpeed = 0f;
            toGround = 0.5f;
        }
        #endregion
    }
}
