﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class OnTrigger : MonoBehaviour
    {
        //從戰鬥系統呼叫 來自敵人的攻擊資訊
        public FightSystem fightSystem;
        public EnemyTarget enemyTarget;
        public float currentHp;
        public bool underAttack;
        public bool ishealth;
        public bool isFallDown;
        public bool isGetWakizashi;
        public Transform attackBox;
        public float damage;
        public float bubbleDamage;
        public int ontriCount;
        public GameObject hit_effect;
        public Animator anim;
        public HitByEnemyEffectPool hitByEnemyEffectPool;
        public int i_getTreasure;

        #region 找各種Timeline
        [Header("各種Timeline播放時，就算被打也不可以扣血")]
        public TimelineManager tm_KyleBeBullied;
        public TimelineManager tm_KyleSpeak;
        public TimelineManager tm_AirShipAppeared;
        public TimelineManager tm_BossAppeared;
        public AI_data boss_data;
        public TimelineManager tm_MissionComplete;
        public TimelineManager tm_NiceKyleBattleMissionStart;
        public TimelineManager tm_elevator;
        public TimelineManager tm_StartExplosion;

        [Header("音效")]
        public GameObject go_mainCamera;
        public PlayerBeHitSoundeffectPool playerBeHitSoundeffectPool;

        public void FindAllTimelineTrigger()
        {
            tm_KyleBeBullied = GameObject.FindGameObjectWithTag("KyleBeBulliedTimeline").GetComponent<TimelineManager>();
            tm_KyleSpeak = GameObject.FindGameObjectWithTag("KyleSpeakTimeline").GetComponent<TimelineManager>();
            tm_NiceKyleBattleMissionStart = GameObject.Find("AnimEvent_03_上層平台Tri").GetComponent<TimelineManager>();
            tm_MissionComplete = GameObject.Find("AnimEvent_07_爆炸過程_Tri").GetComponent<TimelineManager>();
            tm_AirShipAppeared = GameObject.Find("AnimEvent_04_飛船登場_Tri").GetComponent<TimelineManager>();
            tm_BossAppeared = GameObject.Find("AnimEvent_05_boss登場Tri").GetComponent<TimelineManager>();
            boss_data = GameObject.Find("Boss").GetComponent<AI_data>();
            tm_elevator = GameObject.FindGameObjectWithTag("ElevatorRespawn").GetComponent<TimelineManager>();
            tm_StartExplosion = GameObject.FindGameObjectWithTag("StartExplosionTimeline").GetComponent<TimelineManager>();
        }
        #endregion


        public void Start()
        {
            anim = GetComponent<Animator>();
            currentHp = fightSystem.maxHp;
            hitByEnemyEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<HitByEnemyEffectPool>();

            FindAllTimelineTrigger();

            playerBeHitSoundeffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerBeHitSoundeffectPool>();
            go_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        }
        public void Update()
        {
            //Dead(); 

        }
        public void FixedUpdate()
        {

            StartCoroutine(TimeScale());

        }

        public void Dead()
        {
            if (currentHp <= 0)
            {
                currentHp = 0;
                print("你已經死了");
            }

        }
        IEnumerator TimeScale()
        {
            float percentHp = fightSystem.currentHp / fightSystem.maxHp;
            if (underAttack == true && percentHp < 0.7f && percentHp > 0)
            {
                //print("血量已經小於50%");
                Time.timeScale = 0.1f;
                underAttack = false;
                yield return new WaitForSeconds(0.3f);
                Time.timeScale = 1f;
            }
            if (underAttack == true && percentHp <= 0)
            {
                if (currentHp < fightSystem.enemyDamage)
                {
                    //print("被致命一擊");
                    Time.timeScale = 0.1f;
                    underAttack = false;
                    yield return new WaitForSeconds(0.3f);
                    Time.timeScale = 1f;
                }

            }
        }
        public void OnTriggerEnter(Collider other)
        {
            if (tm_AirShipAppeared.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_BossAppeared.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                boss_data.GetHealthRate() <= 0 ||
                tm_KyleBeBullied.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_KyleSpeak.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_StartExplosion.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing )
            {
                return;
            }

            if (other.gameObject.tag == "Enemy_Attack_BOX")
            {
                ontriCount++;
                underAttack = true;

                attackBox = other.transform;// 抓到了敵人的武器
                damage = attackBox.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.GetComponentInParent<AI_data>().int_MelleAttack_Damage;//需要抓父物件並且GetComponentInParent
                                                                                                                                                                                                                 //print("最上層物件: "+ attackBox.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.transform.parent.name);
                currentHp -= damage; //傷害最好別放在Update

                //使用被敵人打到的特效的物件池裡的物件
                hitByEnemyEffectPool.ReUse(transform.position + new Vector3(0, 1, 0), Quaternion.identity);
                //Instantiate(hit_effect, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
                //Destroy(hit_effect, 1.0f);

                //被敵人打到的時候生出被打到的音效物件
                playerBeHitSoundeffectPool.ReUse(go_mainCamera.transform.position, Quaternion.identity);

                //print("敵人攻擊傷害 : " + damage); //表示有抓到傷害
                anim.SetTrigger("take damageTri");
                underAttack = false;


            }

            if (other.gameObject.tag == "Bubble")
            {
                ontriCount++;
                currentHp -= 40;

                //print("主角血量 : " + currentHp);
                //print("被泡泡擊中");
                underAttack = true;
                if (ontriCount % 3 == 0)
                {
                    anim.SetTrigger("take damageTri");
                }
                underAttack = false;
            }

            if (other.gameObject.tag == ("Laser"))
            {
                ontriCount++;
                currentHp -= 50f;
                underAttack = true;
                if (ontriCount % 10 == 0)
                {
                    anim.SetTrigger("take damageTri");
                }
                underAttack = false;
            }

            if (other.gameObject.tag == ("Bomb"))
            {
                ontriCount++;
                currentHp -= 800;

                //print("主角血量 : " + currentHp);
                //print("被炸彈擊中");
                underAttack = true;
                if (ontriCount % 3 == 0)
                {
                    anim.SetTrigger("take damageTri");
                }
                underAttack = false;
            }

            if (other.gameObject.tag == ("JumpShockWave"))
            {
                isFallDown = true;
                underAttack = true;
                if (fightSystem.state.onGround)
                {
                    currentHp -= 1000;
                    anim.SetTrigger("FallDown");
                }

                underAttack = false;

            }

            if (other.gameObject.tag == ("LongLegAttack"))
            {
                hitByEnemyEffectPool.ReUse(transform.position + new Vector3(0, 1, 0), Quaternion.identity);
                isFallDown = true;
                underAttack = true;
                if (fightSystem.state.onGround)
                {

                    currentHp -= 1000 + damage;
                    anim.SetTrigger("FallDown");
                }

                underAttack = false;

            }

            if (other.gameObject.tag == ("Health"))
            {
                if (fightSystem.currentHp > 0)
                {
                    currentHp += 1000f;
                    if (currentHp > fightSystem.maxHp)
                    {
                        currentHp = fightSystem.maxHp;
                    }
                }
            }
            //上層戰鬥中的任務掉落的寶物
            if (other.gameObject.tag == "Treasure")
            {
                i_getTreasure++;
            }

            if(other.gameObject.tag == "Wakizashi" && !isGetWakizashi)
            {

                fightSystem.damage += 1000f;
                isGetWakizashi = true;
                
            }
        }
        public void OnTriggerExit(Collider other)
        {
            //underAttack = false;

            if (other.gameObject.tag == "Enemy_Attack_BOX")
            {
                anim.SetBool("take damage", false);
                underAttack = false;

            }
        }

    }

}

