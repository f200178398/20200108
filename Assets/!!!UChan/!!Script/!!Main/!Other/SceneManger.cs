﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class SceneManger : MonoBehaviour
{
    [Header("電梯上升")]
    public GameObject elevator;
    public GameObject newTop;
    public GameObject top;
    public GameObject down;
    public GameObject elevatorDoor02;
    public GameObject elevatorDoor01; 
    public GameObject elevatordoordown;
    public GameObject doorDown;
    public GameObject doorUp;

    public ElevatorTri elevatorTri;
    public bool isRise;
    public float distance;
    public float firstSpeed;
    public float speed;
    public float doorSpeed;
    public float elevatordoordownSpeed;
    public float temp;
    public Collider riseCol;
    
    [Header("其他樓層")]
    public GameObject otherZone;
    public GameObject zoneOne;
    public GameObject zoneTwo;

    [Header("空氣牆")]
    public Collider wallCol;


    void Start()
    {
        newTop = GameObject.FindGameObjectWithTag("newTop");
        elevator = GameObject.FindGameObjectWithTag("elevator");
        top = GameObject.Find("------ToTop------");
        down = GameObject.Find("------ToDown------");
        elevatorTri = GameObject.Find("Elevator01").GetComponent<ElevatorTri>();
        firstSpeed = Vector3.Distance(top.transform.position, down.transform.position) * speed;
        zoneOne = GameObject.Find("-----------Zone01----------");
        zoneTwo = GameObject.Find("-----------Zone02----------");
        elevatorDoor01 = GameObject.Find("ElevatorDoor01");
        elevatorDoor02 = GameObject.Find("ElevatorDoor02");
        wallCol = GameObject.Find("COL_01").GetComponent<Collider>();
        doorDown = GameObject.Find("DoorDown");
        doorUp = GameObject.Find("DoorUp");
    }

    // Update is called once per frame
    void Update()
    {
        //StartCoroutine(ElevatorRise());
        //StartCoroutine(ColseWallCol());
        //StartCoroutine(NewElevatorRise());
    }

    private void FixedUpdate()
    {
        
    }
    public IEnumerator NewElevatorRise()
    {
        if (elevatorTri.rise == true)
        {
            yield return new WaitForSeconds(10); //等待10秒後上升
            elevator.transform.position = Vector3.Lerp(elevator.transform.position, newTop.transform.position, speed * Time.deltaTime);
            isRise = true;
            speed = NewNewSpeed(); //速度為運行中的速度  temp = 0 ,  速度為0, 不再往上升
        }
    }
    private float NewNewSpeed()
    {
        temp = Vector3.Distance(elevator.transform.position, newTop.transform.position); //計算運行中電梯與目標的距離

        if (temp == 0)
        {
            return temp;
        }
        else
        {
            return firstSpeed / temp;
        }
    }

    public IEnumerator ElevatorRise()
    {
        if(elevatorTri.rise == true)
        {
            yield return new WaitForSeconds(10); //等待10秒後上升
            top.transform.position = Vector3.Lerp(top.transform.position, down.transform.position, speed * Time.deltaTime);
            isRise = true;
            speed = NewSpeed(); //速度為運行中的速度  temp = 0 ,  速度為0, 不再往上升
        }

    }
    private float NewSpeed()
    {
        temp = Vector3.Distance(top.transform.position, down.transform.position); //計算運行中電梯與目標的距離
        
        if(temp == 0)
        {
            return temp;
        }
        else
        {
            return firstSpeed / temp;
        }
    } 
    public void ZoneOneSetActive()
    {
        if(temp == 0 && isRise == true)
        {
            zoneOne.SetActive(false);
        }
    }
    public IEnumerator ColseWallCol()
    {
        yield return new WaitForSeconds(10);
        if (isRise == true && temp == 0)
       {
            wallCol.enabled = false;
       }
    }

    public IEnumerator ElevatorDoorUp()
    {
        if(elevatorTri.rise == true)
        {
            yield return new WaitForSeconds(0.5f);
            elevatorDoor02.transform.position = Vector3.Lerp(elevatorDoor02.transform.position, doorUp.transform.position, doorSpeed * Time.deltaTime);

        }
    }

    public IEnumerator EleveatorDoorDown()
    {
        yield return new WaitForSeconds(10);
        if (isRise == true && temp == 0)
        {
            elevatorDoor01.transform.position = Vector3.Lerp(elevatorDoor01.transform.position, doorDown.transform.position, doorSpeed * Time.deltaTime);
            //elevatorDoor01.SetActive(false);
        }
    }
}
