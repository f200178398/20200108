﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public GameObject elevator;
    public GameObject top;
    public float speed;
    void Start()
    {
        elevator = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, top.transform.position, speed * Time.deltaTime);
    }

    public void OnTriggerEnter(Collider other)
    {
        
    }
}
