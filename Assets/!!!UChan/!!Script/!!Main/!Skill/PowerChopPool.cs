﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerChopPool : MonoBehaviour
{
    void Awake()
    {
        for (int cnt = 0; cnt < initailSize; cnt++)
        {
            GameObject go = Instantiate(powerChopObj[chopIndex]) as GameObject;
            m_pool.Enqueue(go);
            go.SetActive(false);
        }
    }


    [Header("Inititate Settings")]      
    public GameObject[] powerChopObj;
    public int initailSize = 69;
    private int chopIndex;

    private Queue<GameObject> m_pool = new Queue<GameObject>();



    public void ReUse(Vector3 position, Quaternion rotation)
    {
        if (m_pool.Count > 0)
        {
            GameObject reuse = m_pool.Dequeue();
            reuse.transform.position = position;
            reuse.transform.rotation = rotation;
            reuse.SetActive(true);
        }
        else if (m_pool.Count <= 0)
        {
            GameObject go = Instantiate(powerChopObj[chopIndex]) as GameObject;
            go.transform.position = position;
            go.transform.rotation = rotation;
        }
    }


    public void Recovery(GameObject recovery)
    {
        m_pool.Enqueue(recovery);
        recovery.SetActive(false);
    }
}
